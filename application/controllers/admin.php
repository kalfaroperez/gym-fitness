<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MY_Controller {

	/**
	 *
	 */
	public function __construct(){
		parent::__construct();
	}

	public function verplanes($mensaje = null){
		$paramts['CI'] = $this->CI;
		$paramts['leftmenu'] = 'pagina/left_menu';
		$paramts['menu_activo'] = 'Planes';
		$paramts['titulo'] = 'Planes';
		$paramts['contenido'] = 'admin/ver_planes';
		$paramts['desc_titulo'] = 'Vista general de planes';
		$paramts['javascript'] = $this->load->view('admin/js/planesjs','', TRUE);
		$this->load->view('layout/master',$paramts);
	}

	public function wizard($mensaje = null){
		$paramts['CI'] = $this->CI;
		$paramts['leftmenu'] = 'pagina/left_menu';
		$paramts['menu_activo'] = 'Asignar';
		$paramts['titulo'] = 'Asignar';
		$paramts['contenido'] = 'admin/wizard';
		$paramts['mensaje'] = $mensaje;
		$paramts['desc_titulo'] = 'Asignar plan a cliente';
		$paramts['javascript'] = $this->load->view('admin/js/wizardjs','', TRUE);
		$this->load->view('layout/master',$paramts);
	}

	public function citas_aprogramadas(){
		$prog = json_decode(file_get_contents('php://input'), true);
		if($prog['tipoProgramacion'] == 'programada'){

		}

		echo json_encode($prog);
		//$this->load->view('layout/master',$paramts);
	}
}

/* End of file index.php */
/* Location: ./application/controllers/admin.php */
