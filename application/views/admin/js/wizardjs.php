<script type="text/javascript">
    agenda.controller('Wizard', ['$scope', '$http', function($scope, $http) {
        $scope.planSel;  //PLan seleccionado
        $scope.clieSel;  //Cliene seleccionado
        $scope.planes;   // listado de planes
        $scope.clientes; // listado de clientes

        $http.defaults.headers.common.Authorization = 'Basic aW50ZW5zaXR5ZmNAZ21haWwuY29tOjQyYTM2MDcyZjFlM2MxN2YxZjA3';
        var req = {
            method: 'GET',
            url: 'https://app.alegra.com/api/v1/items?start=0&limit=30&order_direction=ASC&metadata=false&query=PLAN',
            cache:true,
            headers:{
                'Content-Type': 'application/json'
            }
        }
        //{method: 'GET', url: $scope.url}
        $scope.color = ['bg-aqua','bg-green','bg-yellow','bg-red','bg-purpure','bg-pink','bg-orange','bg-blue'];

        $http(req).
         then(function(response) {
             $scope.status = response.status;
             $scope.data = response.data;
             $scope.planes = _.sortBy($scope.data, 'reference');
             $scope.listarCliente();
         }, function(response) {
             alert("Request failed");
             //$scope.data = response.data || "Request failed";
             //$scope.status = response.status;
         });

         $scope.listarCliente = function(start = 0, limit = 30){
            var reqcli = req;
            reqcli.url = 'https://app.alegra.com/api/v1/contacts?start='+start+'&limit='+limit+'&order_direction=ASC&metadata=false&query=PLAN&type=client';
            $http(req).
             then(function(response) {
                 $scope.statuscli = response.status;
                 $scope.datacli = response.data;
                 $scope.clientes = _.sortBy($scope.datacli, 'id');
             }, function(response) {
                 alert("Request failed");
                 //$scope.data = response.data || "Request failed";
                 //$scope.status = response.status;
             });
         };

         $scope.seleccionarPLan = function(index) {
            $scope.planSel = $scope.planes[index];
            document.getElementsByClassName("noAccess")[0].style.pointerEvents = 'visible';
            $scope.planDescripcion = JSON.parse($scope.planSel.description.split('\n')[2]);
         };

         $scope.seleccionarClie = function(index) {
            $scope.clieSel = $scope.clientes[index];
            document.getElementsByClassName("noAccess")[1].style.pointerEvents = 'visible';
         };

         $scope.enviar_tarea_programada = function(){
            $scope.tareaProgramada = {
               idPlan:$scope.planSel,
               idClie:$scope.clieSel,
               tipoProgramacion:'programada',
               fecha:$scope.fecha,
               hora:$scope.hora,
               descripcion:JSON.parse($scope.planSel.description.split('\n')[2])
            };
            var request = {
                method: 'POST',
                url: 'http://agenda.intensityfc.com/insertar_citas',
                data:$scope.tareaProgramada,
                headers:{'Content-Type': 'application/json'}
            };
            $http(request).
             then(function(response) {
                 $scope.status = response.status;
                 $scope.data = response.data;
                 console.log($scope.status);
                 console.log($scope.data);
             }, function(response) {
                 alert("Request failed");
                 //$scope.data = response.data || "Request failed";
                 //$scope.status = response.status;
             });
         };
    }]);
</script>
